---
title: DHCP Server (app-dhcp)
published: true
taxonomy:
    category: docs
---


## DHCP Server
The **DHCP Server** app (Dynamic Host Configuration Protocol) allows hosts on a network to request and be assigned IP addresses.  This service eliminates the need to manually configure new hosts that join your network.

## Installation
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_marketplace|Marketplace]].

## Menu
You can find this feature in the menu system at the following location:

<navigation>Network > Infrastructure > DHCP Server</navigation>

## Configuration
### Global Settings
#### Authoritative
Unless you are running more than one DHCP on your network, enable **Authoritative** mode.  When this is enabled, then DHCP requests on unknown leases from unknown hosts will not be ignored.  This will be the case when a foreign laptop is plugged into your network.

#### Domain Name
The server can auto-configure the default Internet domain name for systems using DHCP on your network.  You can either use your own Internet domain (for example: //example.com//) or you can simply make one up (for example: //lan//).  Example:

  * A desktop system on your local network has a system name //scooter// and uses DHCP.
  * The domain name specified in the DHCP server is //example.com//.
  * On startup, the desktop system appends //example.com// to its system name.  Its full hostname would become //scooter.example.com//.

### Subnets
In a typical installation, the DHCP server is configured on all LAN interfaces.  To add/edit DHCP settings for a particular network interface, click on the appropriate add/edit button.

#### Network
The network number is automatically detected by the system.

#### Gateway
This should be automatically set to the interface IP. This is correct if ClearOS is the internet gateway but if ClearOS is just the DHCP server with another device on the LAN as the gateway, this should point to the other device's IP.

#### IP Ranges
Keep a range of IP addresses available for systems and services that require static addresses.  For instance, a [[:content:en_us:7_ug_pptpd|PPTP VPN server]] and some types of network printers require static IP addresses.

In a typical local area network, the first 99 IP addresses are set aside for static addresses while the remaining addresses from 100 to 254 are set aside for the systems using the DHCP server.  Adjust these settings to suit your needs and your network.
<note warning>You must never use the first and last addresses in your subnet. In a /24 subnet (netmask = 255.255.255.0) this means you cannot use the address ending in .0 and .255.</note>

#### DNS Servers
The server can auto-configure the DNS settings for systems using DHCP on your network.  By default, the IP address of the [[:content:en_us:7_ug_dns|DNS Server]] on your ClearOS system is used.  You should change this setting if you want to use an alternate DNS server.

#### WINS Server
If you have a Microsoft Windows Internet Naming Service (WINS) server on your network, you can provide the IP address to all Windows computers on your network.  This will allow Windows systems to access resources via Windows Networking.  You can enter the LAN IP address of your ClearOS system here if you have enabled the WINS server in [[:content:en_us:7_ug_samba|Windows Networking (Samba)]] on your system..

#### TFTP Server
If you have a TFTP server on your network, you can specify the IP address in your DHCP configuration.  Among other uses, TFTP is commonly used by VoIP phones.

#### NTP Server
If you have an NTP (time) server on your network, you can specify the IP address in your DHCP configuration.  Though most modern desktop systems are already configured with a time server, some devices and applications require a local NTP server available.

#### SIP Server
If you have an SIP server on your network, you can specify the IP address in your DHCP configuration.  This sets up a type 120 record in the DHCP server.

#### WPAD
This is URL where to find the wpad.dat file if you are using Web Proxy Auto Discovery. should be in the form <nowiki>http://myserver.mydomain.com/wpad.dat</nowiki> or <nowiki>http://my_wpad_server_IP_address/wpad.dat</nowiki>. If you use an FQDN, it must resolve back to the Server IP address which can be mapped in the [[:content:en_us:7_ug_dns|DNS server app]].
<note tip>It is better to use the IP address form in a Multi-LAN environment to avoid a DNS/hosts file issue</note>

### Leases
A list of systems that are actively using the DHCP server is shown in the **Active Leases** table.  If you would like to make a DHCP lease for a particular system permanent, you can click on the appropriate <button>Edit</button> button in this list and change the lease to Static. You can manually add static leases before they connect to the system using the <button>Add</button> button.\\
Static leases count as active and appear in the Leases table whether they are currently connected or not.
<note>You can change the IP of a lease but it has no effect on the client workstation until the workstation renews the lease.</note>

## Adding other DHCP server options
If you want to add any other DHCP options such as a 176 record for Avaya phones, you can create a file /etc/dnsmasq.d/any_name and in it put something like:
  dhcp-option=enp5s0,120,172.17.2.1

You will need to check your device's manuals for the relevant line

## Common Issues
  * You should only have one (1) DHCP server per network.
  * Enabling the DHCP Server on your Internet connection is... not a good idea. Nor should you do it on any interface with Type = DCHP in the [[content:en_us:7_ug_network|IP Settings]].
{{keywords>clearos, clearos content, DHCP Server, app-dhcp, clearos7, clearos7, userguide, categorynetwork, subcategoryinfrastructure, maintainer_dloper, maintainer_nhowitt}}
